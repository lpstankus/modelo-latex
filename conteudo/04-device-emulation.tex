%!TeX root=../tese.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

\chapter{Device Emulation Implementation}
\label{chp:emulation_impl}

This chapter covers the code implementation of the emulated ADXL313 accelerometer in QEMU. The goal here is to be generalizable to the majority of devices. However, emulation code is highly coupled with the target device and, as the chapter progresses, its sections will gradually turn less general and more specific to the accelerometer. Later sections, especially \ref{sec:interface_impl}, will go very in-depth about the intricacies of the ADXL313 and its implementation. Nevertheless, a significant part of its behaviour is standard, and it can be treated as a role model for emulating other devices with similar complexity.

About the device API, because of the great distinction between buses in the system, they often demand unique sets of requisites from their child devices. Some require little from them, with only initialization and transfer routines, while others demand complex sets of interactions, possibly unique to the particular bus. The difference complicates establishing a common API covering all use cases and tames the emergent code complexity. The API used by peripherals in QEMU's virtual machines is set per bus to mitigate that, where each bus defines its custom interface and set of operations~\cite{armbruster:10}\cite{armbruster:11}. The emulation code for a device varies drastically according to its parent bus. This section will focus on the SSI interface for SPI and SSI devices.

\section{Class}

The buses' custom API is set by a \textbf{class structure}. Classes borrow concepts from object-oriented programming, such as inheritance and virtual methods, to build a model of the available bus configuration and setup for its devices. A class instance is set per device type, covered with details in Section \ref{sec:device_type}, where all the implementations particular for the device are set. Even though they are set in a different manner, the final result of this is similar to inheritance.

The API is defined for a given peripheral by setting the related fields with function pointers to the concrete implementations of the methods. Hence, the bus code calls the function present in the field. The fields may also have default values, which can then be overwritten, similar to how a method can be overridden in object-oriented programming. Function pointers compose the majority of the classes' fields. However, general bus configuration can also be present in them, as they are global to the device type, \textit{e.g.} the polarity of the CS lane in SPI.

For SSI peripherals, the class is set by the \texttt{SSIPeripheralClass} structure, defined in \textit{/include/hw/ssi/ssi.h}. Holding the following fields for device configuration\footnote{From code at: \url{https://gitlab.com/qemu-project/qemu/-/blob/master/include/hw/ssi/ssi.h\#L34}}:

\begin{itemize}
  \item \textbf{parent\_class:} A reference for the parent class (inheritance).
  \item \textbf{cs\_polarity:} Defines whether chip select (CS) exists and if its active high or low.
  \item \textbf{realize:} Function called on device initialization.
  \item \textbf{transfer:} Default function for data transfers, used when the device has standard or no CS behaviour.
  \item \textbf{set\_cs:} Function called at CS line change. Optional and only required when the device has side effects related to the CS line.
  \item \textbf{transfer\_raw:} Function for non-standard CS behaviour, takes control of the CS behaviour at device level. When set, \texttt{transfer}, \texttt{set\_cs} and \texttt{cs\_polarity} fields are unused.
\end{itemize}

The CS line in SPI specifies if the peripheral\footnote{Also known as a slave, however the classic master-slave nomenclature present in SPI and other protocols is being discouraged by the community in favour of the more respectful controller-peripheral counterpart.} is selected for a data transfer. Depending on the device, the line can be high or low in standby, and, in devices with standard behaviour, it would be inverted by the controller to signify the start of a transfer. ADXL313 has standard CS behaviour where the line is set to low on transmission. Thus, only \texttt{realize} and \texttt{transfer} functions will be overwritten.

\section{Device Type}
\label{sec:device_type}

A device implementation is separated into two concepts, a type and its instances. The \textbf{type structure}, in a sense, formalizes the device inside qdev, holding relevant attributes about the device itself, instance metadata, such as size and initialization method, and its class, all aspects global to all instances. A type is defined by the \texttt{TypeInfo} structure in \textit{/include/qom/object.h} and contains a large number of fields to enable extensive customizability for a variety of complex use cases. Different sets of these attributes have to be configured, all according to the custom requisites set by the target device~\cite{qemu:qom}.  SSI buses are fairly straightforward; to create a type for a standard SSI device, the following fields need to be set\footnote{From code at \url{https://gitlab.com/qemu-project/qemu/-/blob/master/include/qom/object.h\#L373}}:

\begin{itemize}
  \item \textbf{name:} The name of the type.
  \item \textbf{parent:} The name of the parent type.
  \item \textbf{instance\_size:} The size of an instance.
  \item \textbf{class\_init:} Function called after all parent class initialization has occurred, allowing a class to set its default virtual method pointers. This is also used to override virtual methods from a parent class.
\end{itemize}

Before they can be used in any form inside QEMU, a user created type must first be declared and registered. The QEMU Object Model (QOM) provides a framework for registering types and instantiating objects. It also implements the object-oriented features found in the class and type structures, such as single-inheritance and multiple inheritances of stateless interfaces.

Types in QOM can be declared and registered in several ways, including defining the macros and calling the internal functions directly. Nevertheless, the most common way is using the \texttt{OBJECT\_DECLARE\_TYPE} macro and its variants for declaration and \texttt{type\_init} and \texttt{type\_register\_static} macros for, respectively, creating a module with a callback function and for registering the type inside the callback~\cite{qemu:qom}. The available functions, as well as their documentation, can all be found at \textit{/include/qom/object.h}. Figure \ref{fig:adxl313_type} contains code snippets for creating, declaring and registering a new SSI device type, where the \texttt{ADXL313State} is a structure holding the device state.

\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
#define TYPE_ADXL313 "adxl313"
OBJECT_DECLARE_SIMPLE_TYPE(ADXL313State, ADXL313)

static void adxl313_class_init(ObjectClass *klass, void *data)
{
	DeviceClass *dc = DEVICE_CLASS(klass);
	SSIPeripheralClass *k = SSI_PERIPHERAL_CLASS(klass);

	k->realize = adxl313_realize;
	k->transfer = adxl313_transfer;
	k->cs_polarity = SSI_CS_LOW;

	set_bit(DEVICE_CATEGORY_INPUT, dc->categories);
}

static const TypeInfo adxl313_info = {
	.name          = TYPE_ADXL313,
	.parent        = TYPE_SSI_PERIPHERAL,
	.instance_size = sizeof(ADXL313State),
	.class_init    = adxl313_class_init,
};

static void adxl313_register_types(void)
{
	type_register_static(&adxl313_info);
}

type_init(adxl313_register_types)
\end{lstlisting}
\end{minipage}
\caption{Code for creating, declaring and registering the ADXL313 type with QOM.}
\label{fig:adxl313_type}
\end{figure}

\section{Device State}

In QOM, instances of a device type are called objects, and they store the values required for emulating the peripheral itself. They represent the device's state. Hence, their types are often referenced as state structures and usually achieve this by mimicking the hardware found in the real-world counterpart, such as registers and memory. Objects also include their peripheral parent structure among their fields. For instance, a SSI device would need to include a \texttt{SSIPeripheral} inside of it (see Figure \ref{fig:adxl313_state}).

\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
struct ADXL313State {
	// private
	SSIPeripheral ssidev;

	// SPI
	uint8_t address;
	uint8_t spi_mode;

	// device registers
	uint8_t bw_rate;
	int16_t accels[3];
	int8_t offsets[3];
};
\end{lstlisting}
\end{minipage}
\caption{State structure of the ADXL313 accelerometer.}
\label{fig:adxl313_state}
\end{figure}

The use of embedded structures in the state structure is a common workaround to implement inheritance in the C programming language using pointer arithmetic. It includes the parent structure inside the child types as one of its fields. With this, the pointer to the parent structure is used. For the cases that have to deal with the individual implementations, the pointer to the wrapper structure can be calculated from the field offset inside the structure. Since the pointers to the parent are all the same type, this abstracts the implementation details of the peripherals in a concise way and memory-efficient format without overruling C's type system~\cite{bhayani:20}.

Pointer arithmetic is often considered dangerous as it can easily lead to undefined behaviour. However, QOM generates macros for calculating the child structure reference from the parent pointer at device declarations, thus making it safe for users. The macro in question is named after the type name given as an argument in the declaration and is callable, taking only the pointer to the parent device as a parameter. Converting the type often is the first line of code in the interfaces' implementations, as the following example:

\begin{verbatim}
ADXL313State *s = ADXL313(dev);
\end{verbatim}

% \subsubsection{Virtual Machine State}

% The state of the peripheral also needs to be described in a \texttt{VMStateDescription} structure. 

% \begin{figure}[]
% \begin{minipage}{\linewidth}
% \begin{lstlisting}
% static const VMStateDescription vmstate_adxl313 = {
% 	.name = "adxl313",
% 	.version_id = 1,
% 	.minimum_version_id = 1,
% 	.fields = (VMStateField[]) {
% 		VMSTATE_SSI_PERIPHERAL(ssidev, ADXL313State),
% 		VMSTATE_UINT8(address, ADXL313State),
% 		VMSTATE_UINT8(spi_mode, ADXL313State),
% 		VMSTATE_UINT8(multiple_read, ADXL313State),
% 		VMSTATE_UINT8(bw_rate, ADXL313State),
% 		VMSTATE_UINT16_ARRAY(accels, ADXL313State, 3),
% 		VMSTATE_UINT8_ARRAY(offsets, ADXL313State, 3),
% 		VMSTATE_END_OF_LIST()
% 	}
% };
% \end{lstlisting}
% \end{minipage}
% \caption{State structure of the ADXL313 accelerometer.}
% \label{fig:adxl313_realize}
% \end{figure}

\section{SSI Interface Implementation}
\label{sec:interface_impl}

The device emulation code presented so far is only related to the peripheral setup. It does not dictate the actual behaviour of the device. The intricacies of how it will work are defined exclusively by the concrete implementations of its bus's interface and can vary according to the user's intention. The behaviour can be guided towards real-world emulation, being as close as possible to the real-world counterpart, or be used as a stub, focusing on predictability to test some part of the pipeline. For this work, the latter makes more sense. Thus, the example ADXL313 emulation uses easily verifiable return values instead of mimicking the actual accelerometer behaviour.

The following paragraphs go in-depth about the ADXL313 implementation of the standard SSI interface and its particularities, which means only the \texttt{realize} and \texttt{transfer} functions will be covered. As expected, this is heavily coupled with the actual device, the most among any other section covered so far. Nevertheless, the foundation concepts of the presented code should be fairly similar, and thus it can be used as a model while targeting other devices with similar complexity.

When creating boards, one of the hardware designers' goals is to minimize wastage. For this reason, bytes and registers tend to hold multiple information. For example, a boolean value can be represented with only one bit, so allocating an entire byte leaves 7 of those bits unused. The $D_i$ notation will be used to reference specific bits of a value, where $i$ indicates the index of bit, from lower to higher bits, starting from zero.

\subsection{Realize}

The \texttt{realize} function is called at device startup, right after object instantiation. Thus it is mainly used to run any initialization procedure of the device and set up its state. The code present in it is often simple and limited to variable initialization, and the argument structure reflects this, taking only a reference to the device and an error pointer for passing failure conditions up the stack. For the ADXL313 implementation, since all fields in the state are set to zero at instantiation, realize only sets arbitrary acceleration values on each axis of the object state (see Figure \ref{fig:adxl313_realize}).

\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
static void adxl313_realize(SSIPeripheral *dev, Error **errp)
{
	ADXL313State *s = ADXL313(dev);

	s->accels[0] = -200;
	s->accels[1] = 0;
	s->accels[2] = 200;
}
\end{lstlisting}
\end{minipage}
\caption{Realize implementation of the ADXL313 accelerometer.}
\label{fig:adxl313_realize}
\end{figure}

% possible to register in vmstate.

\subsection{Transfer}

While the \texttt{realize} function tends to be simple, a lot of the complexity in sensor devices' emulation code comes from the \texttt{transfer} implementation. As the name suggests, it is called at every data transfer of the peripheral, taking as parameters the raw transferred value and a reference to the device. However, while the SPI protocol specifies how to convey data, it does not define how devices ought to grip incoming data. Thus, data handling complexity is left entirely on the device burden. As a result, SPI compliant devices must define a data-transfer layout to communicate with the controller device. Therefore, the device implementation is responsible for parsing the input accordingly, updating the device state when appropriate, and returning an output when expected~\cite{i2c_spi}. The data transfer layout is often unique to each device model, so the implemented parsing procedure for device emulation is tightly coupled with the ADXL313 design.

\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
static uint32_t adxl313_transfer(SSIPeripheral *dev, uint32_t value)
{
	ADXL313State *s = ADXL313(dev);
	uint32_t out = 0;

	switch (s->spi_mode) {
	case ADXL313_SPI_MULREAD:
		if (value == 0) {
			out = adxl313_read(dev);
			s->address += 1;
			break;
		}
		s->spi_mode = ADXL313_SPI_STANDBY;
		/* fallthrough */
	case ADXL313_SPI_STANDBY:
		s->address = value & 0x3F;
		switch (value >> 6) {
		case 0:
			s->spi_mode = ADXL313_SPI_WRITE;
			break;
		case 1:
			// Undefined behaviour for multiple writes
			break;
		case 2:
			s->spi_mode = ADXL313_SPI_READ;
			break;
		case 3:
			s->spi_mode = ADXL313_SPI_MULREAD;
			break;
		}
		break;
	case ADXL313_SPI_READ:
		out = adxl313_read(dev);
		s->spi_mode = ADXL313_SPI_STANDBY;
		break;
	case ADXL313_SPI_WRITE:
		adxl313_write(dev, value);
		s->spi_mode = ADXL313_SPI_STANDBY;
		break;
	}

	return out;
}
\end{lstlisting}
\end{minipage}
\caption{Transfer implementation of the ADXL313 accelerometer.}
\label{fig:adxl313_transfer}
\end{figure}

\begin{figure}[]
\begin{minipage}{\linewidth}
\begin{lstlisting}
static uint32_t adxl313_accel(ADXL313State *s, int i)
{
	// on full resolution, the offsets' scale is 4x the acceleration's scale
	int32_t value = s->accels[i] + (4 * s->offsets[i]);
	return *((uint32_t *) &value); // int from raw bit value
}

static uint32_t adxl313_read(SSIPeripheral *dev)
{
	ADXL313State *s = ADXL313(dev);

	switch (s->address) {
	case ADXL313_REG_DEVID0:
		return 0xad;
	case ADXL313_REG_DEVID1:
		return 0x1d;
	case ADXL313_REG_PARTID:
		return 0xcb;
	case ADXL313_REG_POWER_CTL:
		return 0x4b;
	case ADXL313_REG_DATA_FORMAT:
		return 0x0b;
	case ADXL313_REG_BW_RATE:
		return s->bw_rate;
	case ADXL313_REG_DATA_AXIS_X0:
		return adxl313_accel(s, 0) & 0xff;
	case ADXL313_REG_DATA_AXIS_X1:
		return (adxl313_accel(s, 0) >> 8) & 0xff;
	case ADXL313_REG_DATA_AXIS_Y0:
		return adxl313_accel(s, 1) & 0xff;
	case ADXL313_REG_DATA_AXIS_Y1:
		return (adxl313_accel(s, 1) >> 8) & 0xff;
	case ADXL313_REG_DATA_AXIS_Z0:
		return adxl313_accel(s, 2) & 0xff;
	case ADXL313_REG_DATA_AXIS_Z1:
		return (adxl313_accel(s, 2) >> 8) & 0xff;
	case ADXL313_REG_OFS_AXIS_X:
		return s->offsets[0];
	case ADXL313_REG_OFS_AXIS_Y:
		return s->offsets[1];
	case ADXL313_REG_OFS_AXIS_Z:
		return s->offsets[2];
	}

	return 0;
}

static void adxl313_write(SSIPeripheral *dev, uint8_t value)
{
	ADXL313State *s = ADXL313(dev);

	int8_t raw_int = *((int8_t *) &value); // int from raw bit value

	switch (s->address) {
	case ADXL313_REG_BW_RATE:
		s->bw_rate = value;
		break;
	case ADXL313_REG_OFS_AXIS_X:
		s->offsets[0] = raw_int;
		break;
	case ADXL313_REG_OFS_AXIS_Y:
		s->offsets[1] = raw_int;
		break;
	case ADXL313_REG_OFS_AXIS_Z:
		s->offsets[2] = raw_int;
		break;
	}
}
\end{lstlisting}
\end{minipage}
\caption{Implementation of reads and writes in the emulated ADXL313 accelerometer.}
\label{fig:adxl313_rw}
\end{figure}

ADXL313's transfers are composed of one byte (8 bits), and its datasheet defines as transmission the group of transfers made while the CS line is active. During a full transmission, multiple calls are made to \texttt{adxl313\_transfer} as it is invoked at every transferred byte. Usually, communications with sensor devices are started by a controller device that writes a control value to the communication line. This specifies an operation to the peripheral and indicates how it must treat the following transfers, \textit{e.g.} defining how many bytes are expected to be read from the device. ADXL313 functions under the same logic. All communications with it are started with a byte written by the controller. The control byte then designates if the next operation will be a read or write from the controller's perspective. The controller must set bit $D_7$ to request a read operation or unset it to request a write. Setting bit $D_6$ tells whether multiple bytes will be transferred, while bits $D_5$ to $D_0$ encode the register address for the operation~\cite{adxl313}.

\newpage

A device instance stores the current SPI state in the \texttt{spi\_mode} field, which can be either \texttt{ADLX313\_SPI\_STANDBY}, the initial state waiting for a new control byte transfer, \texttt{ADLX313\_SPI\_READ}, \texttt{ADLX313\_SPI\_WRITE} or \texttt{ADLX313\_SPI\_MULREAD}, for multi-byte reads. Multi-byte writes cause undefined behaviour in the current implementation. Hence, they do not need a state of their own.

Single reads and writes, when $D_6$ is low, operations are composed of only two bytes. The first transfer carries the already detailed control byte. The second byte contains the operation itself, \textit{e.g.} the value to be written in the device address or the value read from it. Single-byte reading and writing are straightforward to implement. First, the device instance parses the control byte, updates the SPI state, then sets the current address pointer. Next, the transfer function calls either \texttt{adxl313\_read} or \texttt{adxl313\_write} to perform the requested operation. Lastly, \texttt{spi\_mode} is reset to standby so forthcoming transfers may be handled (see Figure \ref{fig:adxl313_transfer}).

When $D_6$ is high, the operation has no predefined number of transfers to complete because the end of a multi-read operation is only signaled by the end of the transmission, with the inversion of the chip select line. However, the current QEMU SSI implementation activates the CS line at device initialization and keeps it active throughout the device functioning. This renders it impossible to determine the end of a multi-byte write in the emulation code. The effect of this CS policy is that the ADXL313 device instance operates as if all transfers are done within a single transmission. Yet, as the ADXL313 driver does not make writes with more than one byte, this does not hinder the emulation by any means.

When the controller device issues a read command, the \texttt{value} argument to the \texttt{adxl313\_transfer} function is set to zero. With that, the device instance can identify the end of a multi-byte read request by checking out the value that comes with the call to \texttt{adxl313\_transfer}. While in multi-read mode, read requests are handled as a single read with the difference that the register address pointer is incremented at the end of each transfer. This multi-read feature is convenient for reading several registers in a row. When a non-zero argument is passed to \texttt{adxl313\_transfer}, the state is set back to \texttt{ADLX313\_SPI\_STANDBY}, and the input is parsed as a normal control byte, defining a new operation (see Figure~\ref{fig:adxl313_transfer}).

In the current emulation code, the actual implementation of reads and writes are separated from \texttt{adxl313\_transfer} into distinct functions in order to enhance code readability (see Figure \ref{fig:adxl313_rw}). Both functions resolve the operation with simple lookups using switch cases, where the value in the address pointer is tested against the register addresses present in the \texttt{REG\_} macros. Nevertheless, two particularities found in them are relevant to be discussed.

Firstly, input and output of \texttt{transfer} use unsigned integers; however, acceleration and offsets are defined by signed integers. This causes a problem, as the input and output must match, in the bit level, the C programming language implicitly converts the state integers and these types. A cast is applied to the pointer type instead to avoid any modification done to the bits, which bypasses C's type conversion system. Secondly, in ADXL313's full resolution mode, offsets use a scale four times larger than the acceleration's. Hence, their value must be multiplied by four before they are added.

\section{Emulation as a Development Tool}
\label{sec:devtool}

In summary, all core aspects of an emulated device implementation are now covered. As this chapter goes through many in-depth concepts about QEMU's architecture, the devices inner workings and the used protocol, all of which require a strong background in low-level programming, the chapter itself gets quite dense, the most among the others. It is easy to lose the sense of what this work is aiming to achieve in the midst of it. Given the context, it is best to look at the bigger picture of the state we currently are in and where we are heading.

Using the custom virtual machine set in Chapter \ref{chp:emulation_concepts}, it is possible to test the ADXL313 driver present in Chapter \ref{chp:driver_impl} against the emulated device detailed in this chapter. Doing such confirms that both implementations are functioning properly, as the Linux driver can run its setup and its device access functionality within the VM communication against the emulated ADXL313 with no errors. Using the sysfs interface of the ADXL313 driver also returns the expected values set by the emulated device. In a sense, this confirms that emulation can actually be used to test devices, as the driver pipeline is being entirely tested. Nevertheless, it does not imply that the effort that needs to be put in developing all this infrastructure is a good investment.

