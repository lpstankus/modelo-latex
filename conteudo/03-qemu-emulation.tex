%!TeX root=../tese.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

\chapter{Device Emulation with QEMU}
\label{chp:emulation_concepts}

With the basic understanding of a driver in mind, the following two chapters provide an overview of the code required to emulate a sensor device in QEMU. For such, they will cover the necessary fundamentals and key concepts behind device emulation using QEMU, including relevant code snippets retrieved from an example targeting the ADXL313 accelerometer. The emulation itself is heavily dependant on the target device. However, while the expected behaviour of the code should drastically change between devices, the general guidelines and key concepts presented here should be generalizable for most cases.

This chapter, in particular, will cover the necessary foundations of the QEMU's virtual machine architecture and its device model, and how one can modify the virtual machine in order to add a new device and its dependencies, without going through its implementation.

\section{ADXL313 Overview}

As seen in Chapter \ref{chp:driver_impl}, the current ADXL313 Linux driver only implements standard read/write functionality. It lacks triggers and triggered buffers; thus, no feature that uses interrupt signals is supported at the moment. At this state, the accelerometer in Linux behaves like a polling system for getting the current acceleration, with the added option of setting offsets to the retrieved data. Nevertheless, this simple implementation requires complex interfacing with the IIO core framework and shares the same core concepts as more advanced device drivers.

Considering the current driver state and that the emulation code is as complex as the device it is trying to emulate, the code to support such features is quite simple but still presents some meaningful challenges. From such challenges, it is possible to analyze the viability of emulation with QEMU to aid driver development and testing, making the ADXL313 a proper case study.

\newpage

\section{Devicetree Infrastructure}

Most modern machines rely on auto-configuration protocols, commonly ACPI, to automatically identify connected devices and build a model of the available hardware. They require minimal to no pre-configuration from users and make the computers accessible for non-technical people with 'plug-and-play' style features. However, ACPI raises several questions regarding system performance and security, which are strong roadblocks for some hardware implementations~\cite{corbet:01}. System on a Chip (SoC) and Single Board Computers (SBC) designs usually lack ACPI implementations or any other means of discovering devices, which leaves them with an open problem for modelling the available hardware~\cite{corbet:17}\cite{marta:18}. To solve this, Linux is now favouring the adoption of Devicetrees.

The \textbf{Devicetree (DT)} is a data structure specially designed to describe the available hardware in a device concisely. A Devicetree Source (DTS) file describes the hardware in an expressive and human-readable way. It contains a node-tree data structure where each node represents a single component of an SBC or SoC and may contain child nodes and properties definitions. A node property can hold integer cells, strings, raw bytestrings, hexadecimals, references to other nodes, or it can be left empty. To make DTS development friendlier and make its overall structure more readable, labels can be used as aliases to reference nodes, and external definitions can be included from Devicetree Include Files (DTSI), which may also include definitions from other DTSI files~\cite{devicetreespec}.

A node that describes a particular sensor device is called \textbf{device node}. Since these sensors require device drivers to work properly, device nodes are set to have a \texttt{compatible} property that holds one or more strings specifying the device model compatibility. The order of the strings is important as it sets a matching priority, from most specific to most general, and thus allows for setting compatibility with families of device drivers. The recommended format for the compatible strings is \textit{"manufacturer,model"}, where \textit{manufacturer} identifies the manufacturer of the chip and \textit{model} stands for the device model name or number, as the following example:

\begin{verbatim}
compatible = "mediatek,mt2701-cirq", "mediatek,mtk-cirq";
\end{verbatim}

For device nodes with this \texttt{compatible} list, the first match attempt will be against a device driver compatible with \textit{mediatek,mt2701-cirq}. If no such driver is found, then the list follows on, and a match will be tried against drivers compatible with \textit{mediatek,mtk-cirq}. If again no driver is found, the node can be ignored. Figure~\ref{fig:adxl313-dt-node} shows a device node example for an ADXL313 digital accelerometer.

\lstc
\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/interrupt-controller/irq.h>
spi {
    #address-cells = <1>;
    #size-cells = <0>;

    /* Example for a SPI device node */
    accelerometer@0 {
        compatible = "adi,adxl313";
        reg = <0>;
        spi-max-frequency = <5000000>;
        interrupt-parent = <&gpio0>;
        interrupts = <0 IRQ_TYPE_LEVEL_HIGH>;
        interrupt-names = "INT1";
    };
};
\end{lstlisting}
\end{minipage}
\caption{A DTS device node example for the ADXL313 digital accelerometer using the SPI protocol.}
\label{fig:adxl313-dt-node}
\end{figure}

The raw DTS format is great for human readability but is prohibitively expensive to be stored and processed at boot time, so it needs to be compiled to a better-suited format first by the Devicetree Compiler (DTC). The compilation output is a flat binary encoding of the Devicetree called Flattened Devicetree or Devicetree Blob (DTB). The binary encodes the entire Devicetree in a single, linear, pointerless data structure that is small enough to fit in the bootloader and fast enough to process at boot time~\cite{devicetreespec}.

On Linux, the blob is passed to the kernel at boot time by the bootloader, or it is wrapped up with the kernel image, the latter case for supporting non-DT aware firmware~\cite{linux_devicetree}. Early in the boot, the kernel parses the DTB to identify the machine and execute the platform-specific code required. Later in the kernel initialization, the blob is utilized again to retrieve the list of all device nodes, which are then used to populate the Linux Device Model with data about the platform.

%% Paulo: falta um melhor fechamento/link que o que vem depois, mas como irá mover para o próximo capítulo, creio que está bom.

\section{QEMU}

QEMU is a generic and open-source machine emulator and virtualizer. An emulator enables a computer, called host, to run programs targeted for another computer, called guest. Following the trail of thought, a machine emulator aims to emulate an entire computer, which enables running a whole operating system and its applications in a virtual machine (VM). QEMU mainly implements full system emulation for several computer architectures, including x86, arm, and PowerPC. It also supports computer virtualization and user-space emulation for Linux machines~\cite{qemu:05}.

With the architectures, QEMU includes complete configuration for many boards, such as various Raspberry Pi models. These configurations are virtual machines modelled based on real-world board specifications and thus can emulate the intricacies of the board, which would save a developer from having to test code on real hardware. Documentation for the supported machine specifications and architectures can be found inside the system documentation\footnote{\url{https://gitlab.com/qemu-project/qemu/-/tree/master/docs/system}} directory at QEMU's source code repository.

One of the emulated specifications is \textit{virt}, a generic virtual machine that, on the contrary, does not target any real-world hardware specification. Most architectures in QEMU have a \textit{virt} machine, and they tend to be very minimal, including only the necessary to get a system running. Such machines are targeted for use cases where reproducing the particularities and limitations of a bit of hardware is not required~\cite{docs_qemu:arm_virt}.

Using QEMU as a tool for assisting developers is not a novel idea. Given its capabilities and open-source nature, there has been a growing interest in using QEMU to build tools and frameworks in complex use cases~\cite{luo2012qsim}\cite{dovgalyuk2017qemu}\cite{holler2015qemu}. It enables programmers to have low-level control of the hardware and gives implementations for most typical computer architectures, making it great for code inspection and hand-crafted simulated environments. With that, QEMU presents itself as a perfect candidate for any exploratory work using virtual machines.

As this work aims to analyze how emulation can aid the development and testing of device drivers using virtual machines, the particularities of any device other than the actual test target is out of scope. There are fewer moving parts in virtual architectures' hardware because of their lean configuration. Thus they tend to be easier to modify. Given that, \textit{virt} specs shine as the perfect candidates for emulating sensors. The best option would be an arm-based architecture since the ADXL313 accelerometer is mainly targeted at SoCs and SBCs based on it. Out of the bunch, \textit{aarch64} is the easiest to configure custom kernel versions and, because of that, is the one used in the code examples. One caveat to note, by default arm \textit{virt} uses a 32-bit cortex-a15 CPU, so a custom one with 64-bit support must be explicitly set to use the \textit{aarch64} architecture~\cite{docs_qemu:arm_virt}.

For \textit{virt} machines, the hardware is built at VM startup, and the virtual machine specs are hard-coded in the source; hence changing the hardware configuration requires editing its source code. This forces users to understand some concepts of its architecture to make simple modifications to the machine~\cite{docs_qemu:arm_virt}. Since this chapter aims to explain the code required for device emulation, the required changes for adding a virtual device will also be covered in later sections. However, QEMU is a highly complex software with many features, so going in-depth about them is significantly outside of scope, and the snippets will focus closely only on the required concepts.

QEMU's documentation has gone a long way in recent years, but it still lacks in some ways. Platforms tend to have their overall structure well documented with information about their devices and particularities of the board implementation, while QEMU's internal details are often overlooked. The lack of official information about such details blurs the line of what is architecture or board specific to what is global to QEMU. Therefore, even though the intricacies of QEMU covered in the following sections are mostly global, some will be exclusive to arm \textit{virt}.

\section{Qdev}

In QEMU, the virtual devices are organized using a device model abstraction called \textbf{qdev} (exemplified in Figure \ref{fig:qdev_example}). They are managed in a tree-like data structure of devices connected by buses. Qdev offers a common API for virtual devices with generic configuration and control for different use cases while keeping the overall model conceptually simple~\cite{armbruster:10}.

\begin{figure}[ht!]
  \includegraphics[width=0.75\textwidth]{figuras/qdev_tree_example.png}
  \caption{An example tree of a device structure using qdev~\cite{armbruster:10}.}
  \label{fig:qdev_example}
\end{figure}

In this abstraction, a bus is responsible for handling data transfers. All devices must have a bus as their parent, and buses must be implemented by devices. This relationship between buses and devices builds the overall structure of the Qdev tree. Taking an example from Figure \ref{fig:qdev_example}, the PIIX4\_PM device implements an I\textsuperscript{2}C bus, which in turn holds several other devices. The only exception to the general rule is the \texttt{main-system-bus}, the root bus for the device model tree in QEMU. A single bus can have multiple children devices, and all communication done by them is handled by the direct parent bus. Usually, the children devices would also have local addresses within the scope of their parent~\cite{docs_qemu:qdev}\cite{armbruster:10}.

To have a particular transfer protocol inside a VM in QEMU, a bus must be implemented by one of the machine's devices, usually an emulated bus controller. Despite that, to increase the security against attacks from the guest machine and to reduce the risk of the virtual machine being broken by any kernel updates, \textit{virt} machines in QEMU tend to avoid having many controllers. To emulate sensor devices, this comes up as an issue as most of them use either the SPI or I\textsuperscript{2}C transfer protocols, both lacking in the default configuration of most \textit{virt} machines. Therefore, to add a sensor to the virtual architecture, one device that implements the correct bus protocol must be added first.

\section{System Bus Device}

Even though a bus and a device are conceptually distinct, in practice, devices that implement a bus are wrapped into one single entity called \textbf{system bus device} (sysbus). As sysbus devices include both standard devices and buses, their initialization is more complex. Along with the standard device emulation code, they require their mapped region of memory in the I/O address space and an I/O pin to signal interrupt requests (IRQ).

There would be little to no difference choosing either SPI or I\textsuperscript{2}C as the data transfer protocol to emulate a sensor device; both are plausible to work within the VM. Also, QEMU already has implementations of sysbus devices for either of them, leaving no need to write the emulation code from scratch. Therefore, there should not be much difference between each protocol in terms of VM structure. The main decider between them should be what does the target device support. If a device supports both, as ADXL313 does, either protocol suffice. Given that, the code examples will use QEMU's implementation of the pl022\footnote{\url{https://datasheets.globalspec.com/ds/4439/ARM/8A846544-6043-483B-BF18-579EE30A8CC1}} bus microcontroller to handle SPI data transfers. The following subsections will get in-depth about memory-mapped regions for device I/O and interrupt generation.

\subsection{Memory Map}

One common abstraction of peripheral device communication is memory-mapped I/O (MMIO). Some regions of the addressable memory space are allocated for a particular I/O device. Instead of a typical read/write to memory, when these addresses are accessed, the hardware generates a data access request and sends it to the device. The request is then processed by the peripheral, and the response is given as a regular memory operation from the user's perspective. As it changes the usable memory layout, the BIOS usually creates MMIO regions at boot time and rarely changes during regular operation. The operating system also has to interface with them to properly manage the system RAM without any conflict~\cite{encyclopediaIS}\cite{madieu:20}.

QEMU uses MMIO for most I/O device communication, with memory addresses defined during the VM initialization. Buses are bound to function under MMIO operation, requiring a memory address region at creation, validated and reserved for it. A memory region in QEMU is defined by the \texttt{MemMapEntry} structure, which includes the base memory address and the region's size. One important distinction is that an \texttt{MemMapEntry} only holds the necessary information to identify the area of the memory, but by itself, the structure does not interact with it~\cite{docs_qemu:arm_virt}\cite{qemu:memory_api}.

QEMU enforces safety measures while registering the address in the memory. Unless explicitly set, allocated MMIO areas cannot overlap; for such, it would cause request duplication between the devices on writes and possibly undefined behaviour on reads.  Likewise, mappings can be set to follow general memory region rules according to their application, defined by the architecture and board specification. For instance, the arm \textit{virt} defines the following region map for the memory addresses\footnote{From code at: \url{https://gitlab.com/qemu-project/qemu/-/blob/master/hw/arm/virt.c\#L120}}:

\begin{itemize}
\item Between 0 and 128MB: reserved for flash devices (includes boot device)
\item Between 128MB and 256MB: reserved for miscellaneous device I/O
\item Between 256MB and 1GB: reserved for PCI support, but yet to be implemented
\item Over 1GB: reserved for normal RAM
\end{itemize}

\lstc
\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
static const MemMapEntry base_memmap[] = {
    /* Space up to 0x8000000 is reserved for a boot ROM */
    [VIRT_FLASH] =              { 0x00000000, 0x08000000 },
    [VIRT_CPUPERIPHS] =         { 0x08000000, 0x00020000 },
    /* GIC distributor and CPU interfaces sit inside the CPU peripheral space */
    [VIRT_GIC_DIST] =           { 0x08000000, 0x00010000 },
    [VIRT_GIC_CPU] =            { 0x08010000, 0x00010000 },
    [VIRT_GIC_V2M] =            { 0x08020000, 0x00001000 },
    [VIRT_GIC_HYP] =            { 0x08030000, 0x00010000 },
    [VIRT_GIC_VCPU] =           { 0x08040000, 0x00010000 },
    /* The space in between here is reserved for GICv3 CPU/vCPU/HYP */
    [VIRT_GIC_ITS] =            { 0x08080000, 0x00020000 },
    /* This redistributor space allows up to 2*64kB*123 CPUs */
    [VIRT_GIC_REDIST] =         { 0x080A0000, 0x00F60000 },
    [VIRT_UART] =               { 0x09000000, 0x00001000 },
    [VIRT_RTC] =                { 0x09010000, 0x00001000 },
    [VIRT_FW_CFG] =             { 0x09020000, 0x00000018 },
    [VIRT_GPIO] =               { 0x09030000, 0x00001000 },
    [VIRT_SECURE_UART] =        { 0x09040000, 0x00001000 },
    [VIRT_SMMU] =               { 0x09050000, 0x00020000 },
    [VIRT_PCDIMM_ACPI] =        { 0x09070000, MEMORY_HOTPLUG_IO_LEN },
    [VIRT_ACPI_GED] =           { 0x09080000, ACPI_GED_EVT_SEL_LEN },
    [VIRT_NVDIMM_ACPI] =        { 0x09090000, NVDIMM_ACPI_IO_LEN},
    [VIRT_PVTIME] =             { 0x090a0000, 0x00010000 },
    [VIRT_SECURE_GPIO] =        { 0x090b0000, 0x00001000 },
    [VIRT_MMIO] =               { 0x0a000000, 0x00000200 },
    [VIRT_SPI] =                { 0x0b100000, 0x00001000 },
    /* ...repeating for a total of NUM_VIRTIO_TRANSPORTS, each of that size */
    [VIRT_PLATFORM_BUS] =       { 0x0c000000, 0x02000000 },
    [VIRT_SECURE_MEM] =         { 0x0e000000, 0x01000000 },
    [VIRT_PCIE_MMIO] =          { 0x10000000, 0x2eff0000 },
    [VIRT_PCIE_PIO] =           { 0x3eff0000, 0x00010000 },
    [VIRT_PCIE_ECAM] =          { 0x3f000000, 0x01000000 },
    /* Actual RAM size depends on initial RAM and device memory settings */
    [VIRT_MEM] =                { GiB, LEGACY_RAMLIMIT_BYTES },
};
\end{lstlisting}
\end{minipage}
\caption{Memory map with a VIRT\_SPI entry.}
\label{fig:virt_memmap}
\end{figure}

On some virtual architectures, usually the ones with more complex device structures, it is common to centralize the \texttt{MemMapEntry} structures in a map of some sort. Since the map only holds the entry structures, it does not provide any checks or validation of the regions. However, moving them all to a central location helps the overall code organization and simplifies dealing with the memory layout. A map is usually set by an array of \texttt{MemMapEntry}, where the index serves as the identifier for the region itself. Macros can cover the indexes to avoid magic numbers in the code.

In arm \textit{virt} two arrays are used to map the memory, one for the lower memory, for addresses up to RAM and used for most cases, and one for the higher memory, after RAM and used for some exceptional use cases. Figure \ref{fig:virt_memmap} exhibits the low memory map of the architecture, in it \texttt{VIRT\_SPI} stores the index of the entry for the SPI controller.

\subsection{Interrupt Request Map}

An interrupt request (IRQ) is conceptually simple. It is a hardware signal passed to an interrupt controller, which temporarily stops the running program on the CPU and launches another, the interrupt handler. Interrupts are fundamental to computing because they allow hardware devices to signal when they need to be processed without constantly polling the device for updates, for example, checking when there is a keypress on a keyboard. Computers nowadays have multiple interrupt lines where IRQs can be sent through with specific functionalities, allowing more intelligent decisions by the handler~\cite{encyclopediaIS}.

QEMU implements interrupt after the hardware counterpart. They are just signals sent through an emulated I/O pin, usually a General Purpose Input/Output (GPIO) pin. Consequently, setting up an IRQ for a sysbus device consists of choosing the correct pin to use for its interrupts. The interrupt controllers are modelled after their real-life counterparts, including their set of intricacies, so the chosen interrupt line would vary according to each controller and require case by case analysis. There is a function called \texttt{qdev\_get\_gpio\_in} to get a GPIO pin from the controller, which takes as arguments a sysbus device to take the pin from and an integer with the pin id within the bus. For the case of interrupt controllers, the pin id is usually the lane number.

For arm \textit{virt}, the interrupt controller is the Generic Interrupt Controller (GIC) v3\footnote{\url{https://developer.arm.com/ip-products/system-ip/system-controllers/interrupt-controllers}}. GIC v3 handles a large number of configurable interrupt lanes and, with the limited amount of devices in arm \textit{virt}, most of them end up free for use. A similar array to the memory map is set for the interrupt lanes identifiers to organize the controllers' pins, identifying the taken lanes (see Figure \ref{fig:virt_irq}). The \texttt{qdev\_get\_gpio\_in} function should be called with the interrupt controller followed by the identifier as parameters to get the pin for a particular lane identifier.

\begin{figure}[H]
\begin{minipage}{\linewidth}
\begin{lstlisting}
static const int a15irqmap[] = {
    [VIRT_UART] = 1,
    [VIRT_RTC] = 2,
    [VIRT_PCIE] = 3, /* ... to 6 */
    [VIRT_GPIO] = 7,
    [VIRT_SECURE_UART] = 8,
    [VIRT_ACPI_GED] = 9,
    [VIRT_SPI] = 10,
    [VIRT_MMIO] = 16, /* ...to 16 + NUM_VIRTIO_TRANSPORTS - 1 */
    [VIRT_GIC_V2M] = 48, /* ...to 48 + NUM_GICV2M_SPIS - 1 */
    [VIRT_SMMU] = 74,    /* ...to 74 + NUM_SMMU_IRQS - 1 */
    [VIRT_PLATFORM_BUS] = 112, /* ...to 112 + PLATFORM_BUS_NUM_IRQS -1 */
};
\end{lstlisting}
\end{minipage}
\caption{Interrupt request map (irqmap) with a VIRT\_SPI entry.}
\label{fig:virt_irq}
\end{figure}

With an I/O pin for interrupts and a memory region, adding a device to the VM is just a function call of \texttt{sysbus\_create\_simple}. It takes as arguments, in order, a string to identify the sysbus device, a \texttt{MemMapEntry} for the allocated region of memory and a pin for IRQ requests, as in Figure \ref{fig:pl022_bus}, where \texttt{vms} represents the virtual machine state of the current architecture. For arm \textit{virt} the structure is defined in \textit{/include/hw/arm/virt.h}. Going in-depth about the internal representation of a VM's state is outside of scope, although, for context, the \texttt{memmap} field is an array similar to the one in Figure \ref{fig:virt_memmap}, holding the entries of the memory map of the VM, and \texttt{gic} is the interrupt controller device.

\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
DeviceState *dev = sysbus_create_simple(
    "pl022",
    vms->memmap[VIRT_SPI].base,
    qdev_get_gpio_in(vms->gic, vms->irqmap[VIRT_SPI])
);
\end{lstlisting}
\end{minipage}
\caption{Creation of a pl022 sysbus device.}
\label{fig:pl022_bus}
\end{figure}

This whole process is equivalent to physically connecting the device to the computer. Since only a few arm architectures implement auto-discovery protocols for devices and arm \textit{virt} is not one of them, the operating system cannot identify the newly connected device. The device would also need to be described in the devicetree of the VM to achieve that.

\subsection{Devicetree}

In the virtual architectures of QEMU, instead of using a pre-compiled DTB file, the flatted device tree (fdt) is generated at VM initialization. QEMU implements a custom framework to generate the fdt from the C source code, which is then passed on to the operating system. Having the devicetree generated in the code simplifies the maintenance of the machines and gives developers more tools at their disposal, such as functions for getting guaranteed unique node handles at runtime~\cite{docs_qemu:arm_virt}. The fdt manipulation API follows quite closely the structure of the devicetree and can be transpiled almost directly to a DTS equivalent. It has been made this way to improve the developers' workflow for adding new devices to the architecture since almost certainly they would already have a DTS example from which they could use as a model. 

The code and API of the framework can be found at the \textit{/dtc/libfdt/} directory in QEMU's source code repository. Its functions are all prefixed with \texttt{qemu\_fdt} and most of them perform modifications to the proper fdt, for instance \texttt{qemu\_fdt\_add\_subnode}, adds a subnode to the tree, or \texttt{qemu\_fdt\_setprop\_cell}, sets a property of a node with a cell. The functions also have the same argument structure, where the first one is the target fdt, the second is a string with the path of nodes from the root to the node in question, and the latter arguments are related to the operation itself. An example of code and the DTS equivalent can be seen at Figures \ref{fig:node_example_code} and \ref{fig:node_example_dts}, respectively.

\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
qemu_fdt_add_subnode(fdt, "/parent");
qemu_fdt_setprop(fdt, "/parent", "property-a", "a string", sizeof("a string"));
qemu_fdt_add_subnode(fdt, "/parent/child");
qemu_fdt_setprop_cell(fdt, "/parent/child", "property-b", 0x10);
\end{lstlisting}
\end{minipage}
\caption{Example code of fdt manipulation.}
\label{fig:node_example_code}
\end{figure}

\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
parent {
    property-a = "a string";
    child {
        property-b = <0x10>;
    };
};
\end{lstlisting}
\end{minipage}
\caption{DTS equivalent to code in Figure \ref{fig:node_example_code}.}
\label{fig:node_example_dts}
\end{figure}

Even though simpler devices can usually work with only one node in the devicetree, some require more complex setups and thus need more nodes for a proper configuration. Controllers usually follow the latter case, having aspects such as voltage, clocks and so forth to be configured within the system. For example, the pl022 microcontroller requires a system clock and a voltage regulator within the expected bounds established in its datasheet. Figure \ref{fig:virt_pl022_node_code} shows the code used to configure the pl022 devicetree node in QEMU's fdt framework, where \texttt{ms} is the machine state, the parent structure of the previous virtual machine state. It holds the current hardware state that is common to all platforms in QEMU, such as valid CPUs and RAM, and is defined in \textit{/include/hw/boards.h}

\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
// mclk node
qemu_fdt_add_subnode(ms->fdt, "/mclk");
qemu_fdt_setprop_string(ms->fdt, "/mclk", "compatible", "fixed-clock");
qemu_fdt_setprop_cell(ms->fdt, "/mclk", "#clock-cells", 0x0);
qemu_fdt_setprop_cell(ms->fdt, "/mclk", "clock-frequency", 24000);
qemu_fdt_setprop_string(ms->fdt, "/mclk", "clock-output-names", "bobsclk");
uint32_t clk_phandle = qemu_fdt_alloc_phandle(ms->fdt);
qemu_fdt_setprop_cell(ms->fdt, "/mclk", "phandle", clk_phandle);

// regulator node
const char *reg_node = "/regulator@0";
qemu_fdt_add_subnode(ms->fdt, reg_node);
qemu_fdt_setprop(ms->fdt, reg_node, "compatible", "regulator-fixed", sizeof("regulator-fixed"));
qemu_fdt_setprop_cell(ms->fdt, reg_node, "reg", 0);
qemu_fdt_setprop_cell(ms->fdt, reg_node, "regulator-min-microvolt", 3000000);
qemu_fdt_setprop_cell(ms->fdt, reg_node, "regulator-max-microvolt", 3000000);
qemu_fdt_setprop_string(ms->fdt, reg_node, "regulator-name", "vcc_fun");
uint32_t reg_phandle = qemu_fdt_alloc_phandle(ms->fdt);
qemu_fdt_setprop_cell(ms->fdt, reg_node, "phandle", reg_phandle);

// SPI node
char *spi_node = g_strdup_printf("/spi@%" PRIx64, vms->memmap[VIRT_SPI].base); // spi@address
qemu_fdt_add_subnode(ms->fdt, spi_node);
qemu_fdt_setprop_sized_cells(ms->fdt, spi_node, "reg",
                             2, vms->memmap[VIRT_SPI].base,
                             2, vms->memmap[VIRT_SPI].size);
qemu_fdt_setprop_string(ms->fdt, spi_node, "clock-names", "apb_pclk");
qemu_fdt_setprop_cell(ms->fdt, spi_node, "clocks", vms->clock_phandle);
qemu_fdt_setprop_cells(ms->fdt, spi_node, "interrupts",
                       GIC_FDT_IRQ_TYPE_SPI, vms->irqmap[VIRT_SPI],
                       GIC_FDT_IRQ_FLAGS_LEVEL_HI);
qemu_fdt_setprop_cells(ms->fdt, spi_node, "num-cs", 3);
const char compat[] = "arm,pl022\0arm,primecell";
qemu_fdt_setprop(ms->fdt, spi_node, "compatible", compat, sizeof(compat));
\end{lstlisting}
\end{minipage}
\caption{QEMU code for creating the required devicetree nodes for the pl022 microcontroller.}
\label{fig:virt_pl022_node_code}
\end{figure}

% \begin{figure}[ht!]
% \begin{minipage}{\linewidth}
% \begin{lstlisting}
% mclk {
%     clock-output-names = "bobsclk";
%     #clock-cells = <0x00>;
%     clock-frequency = <0x5dc0>;
%     compatible = "fixed-clock";
%     phandle = <0x8003>;
% };

% regulator@0 {
%     regulator-max-microvolt = <0x2dc6c0>;
%     regulator-min-microvolt = <0x2dc6c0>;
%     regulator-name = "vcc_fun";
%     compatible = "regulator-fixed";
%     reg = <0x00>;
%     phandle = <0x8004>;
% };

% spi@b100000 {
%     num-cs = <0x03>;
%     clock-names = "apb_pclk";
%     interrupts = <0x00 0x0a 0x04>;
%     clocks = <0x8000>;
%     compatible = "arm,pl022\0arm,primecell";
%     reg = <0x00 0xb100000 0x00 0x1000>;
% };
% \caption{DTS to setup a pl022 controller and required clock and regulator.}
% \end{minipage}
% \end{lstlisting}
% \label{fig:virt_pl022_dt}
% \end{figure}

\section{Standard Device}

The device creation API in QEMU is unique per bus, hence each has its requirements to create new device instances, the reasoning behind it will be covered in Chapter \ref{chp:emulation_impl}. The only global requisite between all buses is a reference to the parent itself and the name of the peripheral being created. For SPI devices, these are the only arguments for device creation. Moreover, because of the similarities between the SPI and the SSI communication protocol, an SSI bus may be used to handle SPI devices. Given this, Figure \ref{fig:adxl313_device_create} exhibits the code required for creating a new SPI device in the virtual machine, where \texttt{dev} is a sysbus device, a pl022 in this case -- as with sysbuses, creating a device is equivalent to plugging it in the machine. Thus, most arm computers cannot properly recognize it. For it to be identified by the Operating System, a device tree node must be generated for the peripheral (see Figure \ref{fig:adxl313_node_code}).

\begin{figure}[H]
\begin{minipage}{\linewidth}
\begin{lstlisting}
void *bus = qdev_get_child_bus(dev, "ssi");
DeviceState *spidev = ssi_create_peripheral(bus, "adxl313");
if (!spidev) printf("could not create adxl313\n");
\end{lstlisting}
\end{minipage}
\caption{Creation of an ADXL313 device.}
\label{fig:adxl313_device_create}
\end{figure}

\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
const char compat2[] = "adi,adxl313";
char *nodename2 = g_strdup_printf("/spi@%" PRIx64 "/adc@%" PRIx64,
                                  vms->memmap[VIRT_SPI].base, 0x0l);

qemu_fdt_add_subnode(ms->fdt, nodename2);
qemu_fdt_setprop(ms->fdt, nodename2, "compatible", compat2, sizeof(compat2));

qemu_fdt_setprop_sized_cells(ms->fdt, nodename2, "reg", 1, 0x0);
qemu_fdt_setprop_sized_cells(ms->fdt, nodename2, "spi-max-frequency", 1, 1000000);

qemu_fdt_setprop_cell(ms->fdt, nodename2, "interrupt-parent", pl061_phandle);
qemu_fdt_setprop_cells(ms->fdt, nodename2, "interrupts", 4, 3);
\end{lstlisting}
\end{minipage}
\caption{QEMU code for creating a devicetree node for the ADXL313 accelerometer.}
\label{fig:adxl313_node_code}
\end{figure}

Now that the necessary background for adding new devices to QEMU's virtual machines is covered, we may focus on the actual implementation of one.
