%!TeX root=../tese.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

\chapter{Linux Driver Development}
\label{chp:driver_impl}

One of the essential parts of emulation is understanding the target hardware and how it interacts with the surrounding software from a high level. However, to analyze whether it can aid the development of its surroundings, in-depth knowledge of the interactions and how the software is structured becomes essential. Therefore, as this work aims to study how emulation can assist developers in device driver development, it is essential to break down first how one can be implemented. This chapter aims to cover relevant core concepts of driver development in Linux using the Industrial I/O (IIO) subsystem, focusing on how the device is accessed from the system's perspective and the driver's main routines and data flow.

For such, the ADXL313\footnote{\url{https://www.analog.com/en/products/adxl313.html}} IIO driver implementation will be used as a role model. The device is a 3-axis digital accelerometer with a handful of features, including configurable automatic sleep mode and activity and inactivity setups for data capture. Additionally, it works with various connection setups, including 4-wire and 3-wire SPI configurations, as well as standard I\textsuperscript{2}C.

This work does not focus on providing a comprehensive guide to Linux driver development. This chapter gives a high-level view of driver structure and the necessary background. Thus, beginners looking to get into driver development might find this work lacking in some respects. For example, topics such as the kernel build system will not be discussed here. If that is the case, one can find more guidance through Schmitt's work~\cite{schmitt:19}.

\section{Regmap}
\label{sec:regmap}

Regmap is a register access mechanism provided by the Linux kernel that mainly targets SPI, I\textsuperscript{2}C, and memory-mapped registers. It was introduced in version 3.1 of the Linux kernel to allow factorizing and unifying access to devices with different protocols. When set, a regmap provides an abstract access layer to device registers which does not require specifying the access method. To set up a regmap, one must initialize the appropriate configuration structures with data suited to the connection capabilities. All accesses to the device are then wrapped into a unified API\footnote{The complete API can be found at \url{https://github.com/torvalds/linux/blob/master/include/linux/regmap.h}}, regardless of protocol~\cite{madieu17_regmap}\cite{madieu:20}.

Regmaps can leverage driver development for devices with support for multiple protocols. All device access code can be easily abstracted away by setting a regmap at device initialization for the invoked protocol. As access is often the only unique part of each protocol implementation, the driver can be easily split into two conceptual parts, the core code and the protocol-specific initialization and registration. This division leads to cleaner code, with better separation of concerns, and fewer redundancies in general across multiple kernel drivers~\cite{madieu17_regmap}.

\lstc
\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
regmap = devm_regmap_init_i2c(client, &adxl313_i2c_regmap_config);
if (IS_ERR(regmap)) {
	dev_err(&client->dev, "Error initializing i2c regmap: %ld\n",
		PTR_ERR(regmap));
	return PTR_ERR(regmap);
}
\end{lstlisting}
\end{minipage}
\caption{I\textsuperscript{2}C regmap initialization for the ADXL313 driver.}
\label{fig:adxl313_regmap_i2c}
\end{figure}

Given that, the ADXL313 driver is split across three source files, \textit{adxl313\_core.c}, \textit{adxl313\_spi.c}, and \textit{adxl313\_i2c.c}, holding, respectively, the core code and the SPI and I\textsuperscript{2}C specific code. Regmap initialization and configuration are handled only in the latter two. Figure~\ref{fig:adxl313_regmap_i2c} exemplifies a regmap initialization with proper error handling. A function call to the appropriate protocol constructor takes as parameters the device being abstracted and the regmap configuration for it. There are many possible properties one can set to a regmap. The most relevant regmap properties for the ADXL313 driver are:

\begin{itemize}
\item \textbf{reg\_bits:} The number of bits required to describe a register address, similar to the 32-bit and 64-bit definitions in modern machines for memory address space.
\item \textbf{val\_bits:} The number of bits in a register value, therefore the size in bits of the value present in a given register address.
\item \textbf{rd\_table:} A pointer to a \texttt{regmap\_access\_table} structure specifying the valid register ranges for read access.
\item \textbf{wr\_table:} Analogous to \texttt{rd\_table} for write access.
\item \textbf{max\_register:} The maximum valid register address.
\item \textbf{read\_flag\_mask:} A mask to be set in the top bytes of the register address when doing a read.
\end{itemize}

The regmap configuration structures for both SPI and I\textsuperscript{2}C protocols of the ADXL313 driver can be seen in Figure~\ref{fig:adxl313_regmap_config} and the register tables referenced by it in Figure~\ref{fig:adxl313_regmap_tables}.

\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
static const struct regmap_config adxl313_spi_regmap_config = {
	.reg_bits	= 8,
	.val_bits	= 8,
	.rd_table	= &adxl313_readable_regs_table,
	.wr_table	= &adxl313_writable_regs_table,
	.max_register	= 0x39,
	 /* Setting bits 7 and 6 enables multiple-byte read */
	.read_flag_mask	= BIT(7) | BIT(6),
};

static const struct regmap_config adxl313_i2c_regmap_config = {
	.reg_bits	= 8,
	.val_bits	= 8,
	.rd_table	= &adxl313_readable_regs_table,
	.wr_table	= &adxl313_writable_regs_table,
	.max_register	= 0x39,
};
\end{lstlisting}
\end{minipage}
\caption{ADXL313 regmap configurations for SPI and I\textsuperscript{2}C.}
\label{fig:adxl313_regmap_config}
\end{figure}

\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
static const struct regmap_range adxl313_readable_reg_range[] = {
	regmap_reg_range(ADXL313_REG_DEVID0, ADXL313_REG_XID),
	regmap_reg_range(ADXL313_REG_SOFT_RESET, ADXL313_REG_SOFT_RESET),
	regmap_reg_range(ADXL313_REG_OFS_AXIS(0), ADXL313_REG_OFS_AXIS(2)),
	regmap_reg_range(ADXL313_REG_THRESH_ACT, ADXL313_REG_ACT_INACT_CTL),
	regmap_reg_range(ADXL313_REG_BW_RATE, ADXL313_REG_FIFO_STATUS),
};

const struct regmap_access_table adxl313_readable_regs_table = {
	.yes_ranges = adxl313_readable_reg_range,
	.n_yes_ranges = ARRAY_SIZE(adxl313_readable_reg_range),
};

static const struct regmap_range adxl313_writable_reg_range[] = {
	regmap_reg_range(ADXL313_REG_SOFT_RESET, ADXL313_REG_SOFT_RESET),
	regmap_reg_range(ADXL313_REG_OFS_AXIS(0), ADXL313_REG_OFS_AXIS(2)),
	regmap_reg_range(ADXL313_REG_THRESH_ACT, ADXL313_REG_ACT_INACT_CTL),
	regmap_reg_range(ADXL313_REG_BW_RATE, ADXL313_REG_INT_MAP),
	regmap_reg_range(ADXL313_REG_DATA_FORMAT, ADXL313_REG_DATA_FORMAT),
	regmap_reg_range(ADXL313_REG_FIFO_CTL, ADXL313_REG_FIFO_CTL),
};

const struct regmap_access_table adxl313_writable_regs_table = {
	.yes_ranges = adxl313_writable_reg_range,
	.n_yes_ranges = ARRAY_SIZE(adxl313_writable_reg_range),
};
\end{lstlisting}
\end{minipage}
\caption{ADXL313 regmap register read and write tables.}
\label{fig:adxl313_regmap_tables}
\end{figure}

Apart from being able to easily catch invalid device register accesses by the driver, specifying which registers can be written to and read from also enables one to use the kernel debug tools provided by the regmap subsystem. Its debug interface can be found in the sysfs virtual file-system, located at \textit{/sys/kernel/debug/regmap/entry}, where each entry is an active regmap instance. The features provided by the interface often come in handy for driver development. Among them, it provides ways to peek at register values at any given instant. Thus, one can easily compare raw bit values with the actual return values of the driver, making it a crucial validation tool in the developer's toolkit~\cite{madieu:20}.

\section{IIO Channels}

The most important characteristic of the IIO subsystem is its interface. It abstracts the actual communication with the driver, allowing generic user-space code to interface with a particular device~\cite{cameron:18}. For such, IIO defines the concept of channels. A channel groups information about a homogeneous data stream that must be exposed to user-space, either for input or output. Besides the raw bits of data, a channel provides relevant metadata for applications, such as the type of physical quantity being measured, sample frequency, scale and offset values to convert data to S.I units, etc. Among the many properties that a channel may have, ADXL313 makes use of the following~\cite{IIOChannels:KernelDocs}:

\begin{itemize}
  \item \textbf{type:} The data type of the channel measurement, such as voltage, current, temperature, acceleration.
  \item \textbf{address:} A driver-specific identifier of the channel.
  \item \textbf{modified:} Specifies if a modifier is applied to the channel. The type of this is dependant on the channel's type, and the actual modifier is defined in \texttt{channel2}.
  \item \textbf{channel2:} Specifies the modifier to be applied if modified is set. An example modifier is \texttt{IIO\_MOD\_X} to specify the ``x'' axis in axial sensors.
  \item \textbf{info\_mask\_separate:} Indicates what information is exposed to the user-space that is specific to the channel.
  \item \textbf{info\_mask\_shared\_by\_type:} Indicates what information is exposed to the user-space that is shared by all channels of the same type.
  \item \textbf{info\_mask\_shared\_by\_type\_available:} Indicates what availability information is exposed to the user-space that is shared by all channels of the same type.
  \item \textbf{scan\_type:} A custom structure containing relevant information about the data scan itself, such as the endianness of the data and if it is signed or not. The only field used by the ADXL313 driver is \texttt{realbits}, which stores the number of bits used to store the value itself, excluding bits used for padding, for example.
\end{itemize}

ADXL313 is a 3-axis digital accelerometer that captures data for each axis independently. To expose the data of each axis separately, the device driver implements three unique channels. Per-axis configurable hardware offsets for the acceleration data are also exposed per channel.

\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
#define ADXL313_ACCEL_CHANNEL(index, axis) {				\
	.type = IIO_ACCEL,						\
	.address = index,						\
	.modified = 1,							\
	.channel2 = IIO_MOD_##axis,					\
	.info_mask_separate = BIT(IIO_CHAN_INFO_RAW) |			\
			      BIT(IIO_CHAN_INFO_CALIBBIAS),		\
	.info_mask_shared_by_type = BIT(IIO_CHAN_INFO_SCALE) |		\
				    BIT(IIO_CHAN_INFO_SAMP_FREQ),	\
	.info_mask_shared_by_type_available =				\
		BIT(IIO_CHAN_INFO_SAMP_FREQ),				\
	.scan_type = {							\
		.realbits = 13,						\
	},								\
}

static const struct iio_chan_spec adxl313_channels[] = {
	ADXL313_ACCEL_CHANNEL(0, X),
	ADXL313_ACCEL_CHANNEL(1, Y),
	ADXL313_ACCEL_CHANNEL(2, Z),
};
\end{lstlisting}
\end{minipage}
\caption{ADXL313 IIO channels specifications.}
\label{fig:adxl313_iio_channels}
\end{figure}

The raw data gathered from the accelerometer uses its scaling, defined in the datasheet. However, to be meaningful, the constant for converting the data to standard units\footnote{Note that not all measurement types use SI units in IIO, one must check the documentation for the standard unit of a given type of data. In the case of acceleration, the standard IIO unit is the same as SI.} must be accessible and thus exposed as channel metadata. Along with that, ADXL313 devices support configuring the polling rate of the acceleration, which can also be provided as channel metadata. The last two channel attributes are set per device type. However, only a specified set of polling rates can be chosen for the accelerometer; these need to be exposed to user-space as well. The complete channel specification of the ADXL313 driver can be seen in Figure~\ref{fig:adxl313_iio_channels}.

Device drivers created by the Linux Device Model, the unified descriptor of devices in the Linux kernel, expose a user-space interface through sysfs and the ones in IIO are no exception~\cite{corbet:05}. The core framework of the IIO subsystem automatically creates and exposes the device's attributes from the properties of each channel declared by its driver. For an IIO device with index \textit{X}, all its attributes are exposed under the \textit{/sys/bus/iio/iio:deviceX} directory of sysfs, where the attributes filenames are also generated given the channel specification. For instance, the \texttt{channel2} modifier set in the ADXL313 channels identify what axis the channel represents, reflected by the file's nomenclature. A typical ADXL313 device generates the following files and directories under its sysfs directory:
\begin{alignat*}{3}
&\textit{in\_accel\_sampling\_frequency}            &&\hspace{1cm}\textit{in\_accel\_y\_calibbias} &&\hspace{1cm}\textit{of\_node} \\
&\textit{in\_accel\_sampling\_frequency\_available} &&\hspace{1cm}\textit{in\_accel\_y\_raw}       &&\hspace{1cm}\textit{power}    \\
&\textit{in\_accel\_scale}                          &&\hspace{1cm}\textit{in\_accel\_z\_calibbias} &&\hspace{1cm}\textit{subsystem}\\
&\textit{in\_accel\_x\_calibbias}                   &&\hspace{1cm}\textit{in\_accel\_z\_raw}       &&\hspace{1cm}\textit{uevent}   \\
&\textit{in\_accel\_x\_raw}                         &&\hspace{1cm}\textit{name}
\end{alignat*}

For each of these files the kernel subsystems processes the access accordingly, eventually calling either \textit{adxl313\_read\_raw}, \textit{adxl313\_write\_raw} or \textit{adxl313\_read\_avail}, given the touched file and the operation, either reading or writing. To convert a raw value to standard units, IIO defines the following formula: value = (raw + offset) * scale. Therefore, reading from any \textit{in\_accel\_\emph{axis}\_raw} returns the raw bit of acceleration data in a given instant, which can then be multiplied by the value read from \textit{in\_accel\_scale} to get the actual standard unit measurement of the acceleration.

One caveat in the ADXL313 driver is that it does not actually implement offsets from IIO's perspective. By its definition, an offset is applied to the data at the software level, while the ADXL313 offsets are applied at the hardware level. This difference is marked by the different nomenclature of \textit{offset} and \textit{calibbias}. Hence, if a value is written into any \textit{in\_accel\_\emph{axis}\_calibbias}, the offset will be applied automatically to the acceleration data by the device itself, which is not the case for offsets.

Lastly, the device's polling rate can be checked or edited by, respectively, reading from or writing to \textit{in\_accel\_sampling\_frequency}, while the list of valid sampling frequencies can only be retrieved by reading from \textit{in\_accel\_sampling\_frequency\_available}.

\section{Device Private Data}

Most device drivers need to store data particular to the device instance to assist its operation. In IIO, device private data must be encapsulated in a single data structure which is then stored by the generic IIO device at runtime. With this approach, the generic device is used by internal kernel code, which abstracts the internal driver data, and, when needed, the private data structure can be retrieved by a function call. Thus, from an object-oriented perspective, the relationship between the two structures is similar to inheritance, where the \texttt{iio\_dev} is the superclass, and the private data structure is the subclass. The structure in Figure~\ref{fig:adxl313_data} defines the private data of an ADXL313 instance.

\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
struct adxl313_data {
	struct regmap	*regmap;
	struct mutex	lock; /* lock to protect transf_buf */
	__le16		transf_buf ____cacheline_aligned;
};
\end{lstlisting}
\end{minipage}
\caption{Data struct that holds ADXL313 device private data.}
\label{fig:adxl313_data}
\end{figure}

\begin{itemize}
\item \textbf{regmap:} The regmap used for device register access.
\item \textbf{lock:} A mutex to lock the cacheline aligned transfer buffer.
\item \textbf{transf\_buf:} A buffer used for multi-byte transmissions, as it needs to be cacheline aligned for direct memory access (DMA) support.
\end{itemize}

\section{IIO Operations}

The \texttt{iio\_info} structure defines the static device information used by the IIO subsystem at device registering and while handling any incoming request to the device. Among its fields \texttt{iio\_info} stores several objects, such as pointers to IIO attribute-linked functions, general-purpose and event attributes, buffers and triggers.

\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
static const struct iio_info adxl313_info = {
	.read_raw	= adxl313_read_raw,
	.write_raw	= adxl313_write_raw,
	.read_avail	= adxl313_read_freq_avail,
};
\end{lstlisting}
\end{minipage}
\caption{Struct declaring the static information of the ADXL313 driver.}
\label{fig:adxl313_iio_info}
\end{figure}

The static information of the ADXl313 driver sets callbacks for the \texttt{read\_raw}, \texttt{write\_raw} and \texttt{read\_avail} functions (see Figure~\ref{fig:adxl313_iio_info}). In the IIO subsystem, these callbacks are responsible for, respectively, requesting data from the device, pushing incoming data to the device and returning the list of available values for a given type of information. All of them operate based on the device channel definitions, taking as arguments a reference to the device, a reference to the accessed channel specification and the type of information being requested.

Given that the three functions operate on the same parameters, the callbacks have a similar structure between them. First, a lookup table finds the channel type. Then the underlying subroutines are called to perform the desired operation, according to the reference of the channel specification. The action to be performed can be further specialized according to the actual channel attributes. The implementations of those will follow in the following subsections.

\subsection{read\_raw}

Among the three callbacks, \texttt{read\_raw} is the most complex as it deals with the largest amount of distinct types. It handles reads of raw acceleration data (\texttt{IIO\_CHAN\_INFO\_RAW}), of the scale constant for standard unit conversion (\texttt{IIO\_CHAN\_INFO\_RAW}), of the offset applied to the acceleration (\texttt{IIO\_CHAN\_INFO\_CALIBBIAS}) and of the current sampling frequency of the device (\texttt{IIO\_CHAN\_INFO\_SAMP\_FREQ}).

The subroutines for each data type are quite similar, consisting of reading one or more registers of the device, processing the bits accordingly and returning the expected value. The only exception is the scale constant given by the datasheet and can be returned without peeking at the accelerometer. As regmap abstracts the actual device access, the subroutines will differ the most on data processing. For reference, the full implementation of \texttt{adxl313\_read\_raw} can be seen in Figure~\ref{fig:adxl313_read_raw}.

Parsing and processing the raw data gathered from the device can be a cumbersome task. However, as these issues are recurrent around the Linux kernel, it provides general implementations treating data to simplify dealing with parsing and processing. For instance, several functions automatically convert little-endian or big-endian to the actual CPU endianness. Another example is converting raw bits, returned from regmap as unsigned chars, to signed integers. These features are widely used across multiple drivers and dramatically improve the driver developer workflow. For such, ADXL313 uses both of these function sets, as its acceleration data is a 13-bit signed integer stored across two 8-bit registers in little-endian.

However, some processing has to be done per device according to its implementation, even with the extensive kernel infrastructure. In the ADXL313 driver, this happens in two instances. Firstly, the offset scale used internally by the accelerometer is four times larger than the acceleration's scale. Thus, the offset must be multiplied by four before it is passed to user-space to be consistent with the acceleration. Secondly, the sampling frequency of ADXL313 is not a value by itself. It is an index for a table of valid polling rates defined at the device-sheet and described by \texttt{adxl313\_odr\_freqs} in Figure~\ref{fig:adxl313_odr}. Therefore, the raw data must be matched against the table, and the result is the returned value.

\begin{figure}[H]
\begin{minipage}{\linewidth}
\begin{lstlisting}
static int adxl313_read_raw(struct iio_dev *indio_dev,
			    struct iio_chan_spec const *chan,
			    int *val, int *val2, long mask)
{
	struct adxl313_data *data = iio_priv(indio_dev);
	unsigned int regval;
	int ret;

	switch (mask) {
	case IIO_CHAN_INFO_RAW:
		ret = adxl313_read_axis(data, chan);
		if (ret < 0) return ret;

		*val = sign_extend32(ret, chan->scan_type.realbits - 1);
		return IIO_VAL_INT;
	case IIO_CHAN_INFO_SCALE:
		/*
		 * Scale for any g range is given in datasheet as
		 * 1024 LSB/g = 0.0009765625 * 9.80665 = 0.009576806640625 m/s^2
		 */
		*val = 0;
		*val2 = 9576806;
		return IIO_VAL_INT_PLUS_NANO;
	case IIO_CHAN_INFO_CALIBBIAS:
		ret = regmap_read(data->regmap, ADXL313_REG_OFS_AXIS(chan->address), &regval);
		if (ret) return ret;

		/*
		 * 8-bit resolution at +/- 0.5g, that is 4x accel data scale
		 * factor at full resolution
		 */
		*val = sign_extend32(regval, 7) * 4;
		return IIO_VAL_INT;
	case IIO_CHAN_INFO_SAMP_FREQ:
		ret = regmap_read(data->regmap, ADXL313_REG_BW_RATE, &regval);
		if (ret) return ret;

		ret = FIELD_GET(ADXL313_RATE_MSK, regval) - ADXL313_RATE_BASE;
		*val = adxl313_odr_freqs[ret][0];
		*val2 = adxl313_odr_freqs[ret][1];
		return IIO_VAL_INT_PLUS_MICRO;
	default:
		return -EINVAL;
	}
}
\end{lstlisting}
\end{minipage}
\caption{Function for handling raw read operations for the ADXL313 devices.}
\label{fig:adxl313_read_raw}
\end{figure}

\begin{figure}[H]
\begin{minipage}{\linewidth}
\begin{lstlisting}
static const int adxl313_odr_freqs[][2] = {
	[0] = { 6, 250000 },
	[1] = { 12, 500000 },
	[2] = { 25, 0 },
	[3] = { 50, 0 },
	[4] = { 100, 0 },
	[5] = { 200, 0 },
	[6] = { 400, 0 },
	[7] = { 800, 0 },
	[8] = { 1600, 0 },
	[9] = { 3200, 0 },
};
\end{lstlisting}
\end{minipage}
\caption{Sample frequency rate table of the ADXL313 driver.}
\label{fig:adxl313_odr}
\end{figure}

\subsection{write\_raw}

The ADXL313 driver implementation of the \texttt{write\_raw} callback only handles writes of two data types \texttt{IIO\_CHAN\_INFO\_CALIBBIAS} and \texttt{IIO\_CHAN\_INFO\_SAMP\_FREQ}, as seen in Figure~\ref{fig:adxl313_write_raw}.  Both of these subroutines are fairly similar to the \texttt{read\_raw} implementations, but with the inverse data flow. Therefore, a user-space value is received, parsed and then written to the device. For the offset, given the scale difference stated previously, the input must be divided by four before it is written to the device to be consistent. While for the sampling frequency, the input is used for an inverse search on the frequency table to find the index. It is then written onto the device if the search is successful. 

\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
static int adxl313_write_raw(struct iio_dev *indio_dev,
			     struct iio_chan_spec const *chan,
			     int val, int val2, long mask)
{
	struct adxl313_data *data = iio_priv(indio_dev);

	switch (mask) {
	case IIO_CHAN_INFO_CALIBBIAS:
		/*
		 * 8-bit resolution at +/- 0.5g, that is 4x accel data scale
		 * factor at full resolution
		 */
		if (clamp_val(val, -128 * 4, 127 * 4) != val)
			return -EINVAL;

		return regmap_write(data->regmap, ADXL313_REG_OFS_AXIS(chan->address), val / 4);
	case IIO_CHAN_INFO_SAMP_FREQ:
		return adxl313_set_odr(data, val, val2);
	default:
		return -EINVAL;
	}
}
\end{lstlisting}
\end{minipage}
\caption{Function for handling raw write operations for the ADXL313 devices.}
\label{fig:adxl313_write_raw}
\end{figure}

\subsection{read\_avail}

Finally, the \texttt{read\_avail} function is the simplest of the bunch, handling only \texttt{IIO\_CHAN\_INFO\_SAMP\_FREQ}. As the function is only responsible for returning the available values for the sampling frequency, it must only return the values present in \texttt{adxl313\_odr\_freqs} tables. For that, the subroutine only has to return a pointer to the table, its size and data type, as seen in Figure~\ref{fig:adxl313_read_avail}.

\begin{figure}[ht!]
\begin{minipage}{\linewidth}
\begin{lstlisting}
static int adxl313_read_freq_avail(struct iio_dev *indio_dev,
				   struct iio_chan_spec const *chan,
				   const int **vals, int *type, int *length,
				   long mask)
{
	switch (mask) {
	case IIO_CHAN_INFO_SAMP_FREQ:
		*vals = (const int *)adxl313_odr_freqs;
		*length = ARRAY_SIZE(adxl313_odr_freqs) * 2;
		*type = IIO_VAL_INT_PLUS_MICRO;
		return IIO_AVAIL_LIST;
	default:
		return -EINVAL;
	}
}
\end{lstlisting}
\end{minipage}
\caption{Function for handling read available operations for the ADXL313 devices.}
\label{fig:adxl313_read_avail}
\end{figure}

\section{Device Probing and Setup}
\label{sec:probe}

When a new device is discovered by the kernel system, the bus-specific routine is dispatched to find a suitable driver for the device in the known drivers' list. The bus routine then calls its registered probe function if one is identified. The probe is responsible for initializing all aspects of the driver necessary to support the newly discovered device. This includes, for example, allocating memory for the private structures, setting up the device's configuration and registering the device itself in the appropriate subsystem~\cite{corbet:05}.

\begin{figure}[H]
\begin{minipage}{\linewidth}
\begin{lstlisting}
/**
 * adxl313_core_probe() - probe and setup for adxl313 accelerometer
 * @dev:	Driver model representation of the device
 * @regmap:	Register map of the device
 * @name:	Device name buffer reference
 * @setup:	Setup routine to be executed right before the standard device
 *		setup, can also be set to NULL if not required
 *
 * Return: 0 on success, negative errno on error cases
 */
int adxl313_core_probe(struct device *dev,
		       struct regmap *regmap,
		       const char *name,
		       int (*setup)(struct device *, struct regmap *))
{
	struct adxl313_data *data;
	struct iio_dev *indio_dev;
	int ret;

	indio_dev = devm_iio_device_alloc(dev, sizeof(*data));
	if (!indio_dev) return -ENOMEM;

	data = iio_priv(indio_dev);
	data->regmap = regmap;
	mutex_init(&data->lock);

	indio_dev->name = name;
	indio_dev->info = &adxl313_info;
	indio_dev->modes = INDIO_DIRECT_MODE;
	indio_dev->channels = adxl313_channels;
	indio_dev->num_channels = ARRAY_SIZE(adxl313_channels);

	ret = adxl313_setup(dev, data, setup);
	if (ret) {
		dev_err(dev, "ADXL313 setup failed\n");
		return ret;
	}

	return devm_iio_device_register(dev, indio_dev);
}
\end{lstlisting}
\end{minipage}
\caption{Probe function registered for the ADXL313 driver.}
\label{fig:adxl313_core_probe}
\end{figure}

As stated in Section~\ref{sec:regmap}, part of the ADXL313 initialization is protocol specific. Hence, both the I\textsuperscript{2}C and SPI counterparts split the device probe into two separate functions. \texttt{adxl313\_core\_probe} holds the code shared by both protocols, responsible for configuring the device model representation, for allocating and initializing the private device data, for ensuring the device is in a workable state and setting it up for driver use and, finally, for registering it in the IIO subsystem (see Figure~\ref{fig:adxl313_core_probe}). However, as a device initialization is not complete without the protocol-specific part, the core probe is not registered by any device as its probe function.

On the other hand, the protocol-specific probe function is the first part of the initialization process. They are responsible for setting up the device among the subsystem, configuring and initializing a regmap and, lastly, calling the core probe to finish the device initialization. Since the probe of both protocols does the same procedures thing, but for different device types, their code structure is highly similar, exemplified by \texttt{adxl313\_spi\_probe} in Figure~\ref{fig:adxl313_spi_probe}.

\begin{figure}[H]
\begin{minipage}{\linewidth}
\begin{lstlisting}
static int adxl313_spi_probe(struct spi_device *spi)
{
	const struct spi_device_id *id = spi_get_device_id(spi);
	struct regmap *regmap;
	int ret;

	spi->mode |= SPI_MODE_3;
	ret = spi_setup(spi);
	if (ret) return ret;

	regmap = devm_regmap_init_spi(spi, &adxl313_spi_regmap_config);
	if (IS_ERR(regmap)) {
		dev_err(&spi->dev, "Error initializing spi regmap: %ld\n", PTR_ERR(regmap));
		return PTR_ERR(regmap);
	}

	return adxl313_core_probe(&spi->dev, regmap, id->name, &adxl313_spi_setup);
}
\end{lstlisting}
\end{minipage}
\caption{SPI specific probe function of the ADXL313 driver.}
\label{fig:adxl313_spi_probe}
\end{figure}

\section{Summary}

As stated in the introduction of this chapter, this is not a comprehensive guide to Linux driver development. As one may have noticed, a significant part of the intricacies of the code snippets presented throughout this chapter are purposely ignored in favor of a broader view of the code behaviour. Also, several parts of the driver implementation are not present in this chapter, as they would not add much value in terms of exemplifying the driver data flow; even though they are essential for its proper functioning. However, given this broad overview of an IIO device driver, one can have a better understanding of how data is moved to and from the devices by the driver. This is relevant for emulation, as it is now plausible for one to map an abstract action in the operating system, such as reading from a file in sysfs, to its actual consequence in the hardware-level, such as an access to a certain device register, following with the example. With this in mind, we can now follow up with the emulation itself. 
