%!TeX root=../tese.tex ("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

\chapter{Introduction}
\label{chp:introduction}

The Linux\footnote{\url{https://www.linuxfoundation.org/projects/linux/}} operating system kernel is one of the most significant open-source software projects to date and one of the most important collaborative projects in history. It is almost ubiquitous in computing, being a predominant force in almost all of its areas. From powering all current top 500 super computers~\cite{linuxTOP500:data}, the majority of the cellphone market share with Android~\cite{androidmarketshare:data} and about 70\% of devices in IoT applications~\cite{kernelReport:17}, a strong argument can be made that it is the underlying foundation of modern computing.

Linux is surrounded by vibrant communities of contributors. Developers from around the world dive into its development; either for the challenge, for self-improvement or for fun, people make massive amounts of contributions to the Linux source code. From 2005 to 2017 it has received code patches from roughly 15,600 unique developers, ranking it as one of the most popular projects ever~\cite{kernelReport:17}. With this many people actively inspecting and improving the source code, the risk of security issues and user surveillance is minimized. Its open nature foments a sense of community, encouraging knowledge sharing and collaborative development among peers.

An operating system kernel is a software responsible for controlling the hardware operations and managing the machine's resources, providing abstractions for their usage. It is a highly complex software; in particular, Linux's source code has over twenty million lines of code~\cite{kernelReport:17}. To handle its complexity and size, Linux separates its codebase into smaller, maintainable parts called subsystems, each responsible for smaller portions of the functionality provided by the kernel. There are components responsible for handling different hardware devices within many of these subsystems. These programs are called device drivers.

Drivers are fundamental to the functioning of the Linux kernel. A driver is responsible for mapping existing kernel abstractions to the functionality provided by a given device. With the abstractions, user-space programs can leverage the device's resources without dealing with any intricacies of the hardware~\cite{deviceDriverDef}. As this process is heavily dependant on the device specification, each board design must be targeted by a driver to function correctly within the system. With the constant influx of new devices to the market, driver development has become one of the most significant parts of the Linux kernel. Currently, the majority of lines of code in Linux comes from drives, but this brings some issues to light~\cite{chou2001empirical}.

Code in Linux ``hardens'' over time, which means older code tends to have fewer bugs and security risks as more developers take time to examine it. However, device drivers are much more recent than core parts of the kernel. Therefore, they tend to be more prone to vulnerabilities. Also, verifying if a given driver works properly is often troublesome. Validation against physical hardware is difficult. A subsystem might contain dozens or even hundreds of drivers, rendering it almost impossible for maintainers to own every device present in it. Validation against Continuous Integration (CI) frameworks is also problematic. CI for device drivers is still an open problem in Linux. Today, there is no built-in kernel infrastructure, and there are very few frameworks for it, most no longer supported and none enforced by subsystems. In summary, it is possible to state that device driver testing is still precarious. As a result, device drivers end up concentrating more bugs than any other part of the kernel~\cite{chou2001empirical}.

Among the various solutions for the lack of proper testing of device drivers, a short-term possibility is using emulation to assist developers with device testing and development. This approach consists of emulating the target device inside a machine emulation environment. The device driver can then be tested against the emulated peripheral, possibly assisting testing and development of the driver.

\section{Objective}

Using emulation to assist driver development is an idea that has already been in the minds of some relevant members of the Linux communities\footnote{\url{https://lore.kernel.org/linux-iio/20210614113507.897732-1-jic23@kernel.org/}}. However, it is still quite novel; hence few real experiments have been made with it and not many reports are available. It's not clear if device emulation is a good solution for device driver testing, but it might provide a great auxiliary tool while real long-term solutions are still in development.

Given that, this work aims to explore the use of emulation to assist the development and testing of device drivers in Linux, taking the ADXL313 model as example. ADXL313 chips were designed by Analog Devices Inc. to measure acceleration primarily in car alarms and black boxes, as they provide several built-in sensing functions and are quite shock resistant.

\section{Used Conventions}

In this work, the following typographic convention will be used:

\begin{itemize}
\item \textit{italic} will be used to express parts of the file system, including files and directories.
\item \texttt{monospaced} will be used to express parts of the computer source code, such as structures, variables and functions.
\item \textbf{bold} will be used to highlight definitions and emphasize important concepts.
\end{itemize}

\section{Manuscript Structure}

Besides this introduction, the manuscript has five other chapters, organized as follows. Chapter~\ref{chp:driver_impl} introduces the core concepts of a device driver in Linux using the Industrial I/O subsystem's core framework, exemplified by the implementation of the ADXL313 driver in the subsystem. Chapter~\ref{chp:emulation_concepts} presents the fundamental concepts behind device emulation using QEMU, going through a bit of its architecture and device model and necessary background. Chapter~\ref{chp:emulation_impl} follows the previous, focusing on concepts about device emulation implementation itself with an in-depth overview of the ADXL313 emulation code. Chapter~\ref{chp:discussion} provides a discussion about the results of the exploratory work and presents the conclusions and possible future works. Finally, Chapter~\ref{chp:personal} shares the author's personal appreciation about this work.
