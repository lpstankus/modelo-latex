%!TeX root=../tese.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

\chapter{Final Remarks}
\label{chp:discussion}

The main results of this work include the driver for ADXL313 devices, an ADXL313 emulated device using QEMU and, finally, a brief analysis of how the latter can aid the development and testing of the first.

The driver itself was developed following the open-source development best practices in the Linux kernel. Testing the device properly against multiple hardware configurations, gathering feedback from community reviews, implementing the proposed improvements and properly documenting non-trivial code decisions are among the practices adopted during development. As a result, the driver has been accepted by the IIO subsystem and will be globally available in the Linux kernel from version 5.16 onwards\footnote{\url{https://github.com/torvalds/linux/commit/636d44633039348c955947cee561f372846b478b}}\footnote{\url{https://github.com/torvalds/linux/commit/af1c6b50a2947cee3ffffb32aa8debb100c786bb}}. This first version allows users to use ADXL313 devices to read acceleration measurements, set offsets for the acceleration data and change the sampling frequency of the device.

On the other hand, the emulation implementation was developed to be tested against the driver. Its development process was more exploratory than not. Therefore, the final result is not production-ready code, and, consequently, there is no plan to send it out for review in any QEMU community. However, even with the workarounds presented in Chapter \ref{chp:emulation_impl}, it serves its purpose. It can be targeted against the ADXL313 driver implementation without any errors within an emulation environment as it handles all accesses made by the driver.

As stated in Section \ref{sec:devtool}, in a sense, the fact that the emulation against driver setup works confirms that it can be used as a bare minimal testing tool. However, it is highly inconvenient for such. As machine emulation in QEMU performs full system emulation, every kernel part has to be run, as with any regular computer. This approach includes processes that waste time and resources but are not relevant to the parts of the code being tested, such as system boot. The interaction is not automatic; hence the actual developer has to go over the sysfs directory and manually test the read and write accesses of the device. Even the actual implementation of the emulated device is a bit unwieldy, as it requires too much knowledge about QEMU intrinsics. This scenario invalidates its use as a continuous integration tool. Nevertheless, it can still be relevant to assist developers in specific scenarios.

Using QEMU to assist driver development is not a novel idea. Jonathan Cameron, the maintainer of the IIO subsystem, has already declared using a custom QEMU emulation as a target for one of his patch series\footnote{\url{https://lore.kernel.org/linux-iio/20210614113507.897732-1-jic23@kernel.org/}}\footnote{\url{https://github.com/jic23/qemu/blob/ad7280a-hacks/hw/misc/ad7280a.c}}. The work done there consisted mainly of standardizing an old driver's code to the newer conventions of the subsystem. Since Cameron did not have the hardware in his hands for proper testing, he developed an emulation target based on the current driver and refactored it against the emulated device. In a way, his approach is similar to an integration test.

The emulated device implemented in this work had a similar development process as Cameron's. Both targeted an already functioning device driver. Then the working emulation was used for its purpose, in Cameron's case, the refactoring and in mine this brief analysis.

Given the current state, which doubtedly will improve, this is the correct way to use emulation for testing drivers. It is a short-term solution for specific problems, such as refactoring an existing driver while the kernel lacks a proper CI interface. More complex use cases may see QEMU's shortcomings compared to proper testing frameworks.

Building a driver from scratch using emulation is also not a good idea. It would be easy to misinterpret the datasheet and make minor deviations from the real-world device. With a collection of such deviations, the final result might be a device driver that only supports a device's wrong implementation. A driver which supports no device is no more than a useless implementation.

Apart from using it as a reference for building a proper CI for device drivers in Linux, there is not much to go from here with device emulation. It has a purpose in other areas; however, forcing QEMU to be a CI interface does not seem to be a path to go.

Nevertheless, ADLX313 devices have plenty of features that their driver currently lacks support, which can be suggestions for future work. Such features include: 

\begin{itemize}
\item add proper FIFO support
\item add support for interrupt requests for data ready
\item add support for handling trespassing the activity and inactivity thresholds
\item add support for configurable automatic sleep mode
\end{itemize}
